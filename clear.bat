del *.pdf
del Figures\*.pdf
del *.log,*.aux,*.tmp,*.lg
del *.dvi,*.idv,*.4ct,*.4tc,*.xref
del *.synctex,*.synctex.gz
del *.bbl,*.blg
del *.out,*.toc