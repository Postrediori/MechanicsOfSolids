  \chapter{Тензорная алгебра в криволинейных координатах}
  
  При рассмотрении тензоров в косоугольном базисе криволинейной системы
  координат необходимо четко различать верхние и нижние индексы. Суммирование
  ведётся по индексу, который повторяется дважды "--- один раз как верхний
  и один раз как нижний. Свободные индексы должны иметь одинаковое расположение
  в обеих частях равенства. Существует правило ``формула сама себя пишет''.
  
  
  \section{Криволинейные координаты}
  
  \begin{figure}[h!]
    \centering
    \input{Figures/Figure_06}
    \caption{Вектор $\vec{r}$ в криволинейных координатах}
  \end{figure}
  
  Криволинейными координатами называются три числа $x^1, x^2, x^3$, которые
  в совокупности однозначно определяют положение точки в пространстве. Поэтому
  декартовы координаты точки являются однозначными функциями криволинейных
  координат. Причём, уравнения $x_k=x_k(x^1,x^2,x^3), k=1,2,3$ однозначно
  разрешимы относительно криволинейных координат.
  
  \definition{Условия однозначной разрешимости} этой системы уравнений заключаются
  в отличии от нуля якобиана
  \begin{equation}
    \operatorname{det} \Big( \frac{\partial x_l}{\partial x_k} \Big) \neq 0.
  \end{equation}
  
  Предполагается, что криволинейные координаты пронумерованы таким образом,
  что этот якобиан положителен, то есть выполняется условие
  \begin{equation}
    \sqrt{g}
      = \operatorname{det} \Big( \frac{\partial x_l}{\partial x_k} \Big) > 0.
  \end{equation}
  
  
  \subsection*{Координатные линии и поверхности}
  
  В каждой точке пространства определены три координатные поверхности
  $x^k=\const$. Линия пересечения двух координатных поверхностей называется
  \definition{координатной линией}. В каждой точке пространства определены
  три координатные линии:
  \begin{equation*}
    [ x^k ], \quad x^l = \const, x^s = \const, k \neq l \neq s \neq k.
  \end{equation*}
  
  Вдоль каждой координатной линии изменяется только одна криволинейная
  координата.
  
  
  \subsection*{Основной базис}
  
  В каждой точке пространства определены три вектора основного базиса:
  \begin{equation}
    \vr_k = \frac{\partial \vr}{\partial x^k}.
  \end{equation}
  
  Векторы основного базиса направлены по касательной к соответствующим
  координатным линиям. Смешанное произведение векторов основного базиса:
  \begin{equation}
    \vr_1 \cdot (\vr_2 \times \vr_3) = \sqrt{g} > 0.
  \end{equation}
  
  Следовательно, векторы основного базиса некомпланарны и образуют
  правую тройку.
  
  
  \subsection*{Векторы взаимного базиса. Взаимный базис}
  
  В каждой точке пространства определены три вектора взаимного базиса:
  \begin{equation}
    \vr^k = \frac{1}{2 \sqrt{g}} \ e^{k l s} \ \vr_l \times \vr_s,
  \end{equation}
  \noindent где $e^{k l s}$ "--- символы Леви-Чивитта, принимающие значения
  $-1,0,1$. Выпишем выражения для векторов $\vr_1, \vr_2, \vr_3$:
  \begin{align*}
    \vr^1 &= \frac{1}{\sqrt{g}} \ \vr_2 \times \vr_3, \\
    \vr^2 &= \frac{1}{\sqrt{g}} \ \vr_3 \times \vr_1, \\
    \vr^3 &= \frac{1}{\sqrt{g}} \ \vr_1 \times \vr_2.
  \end{align*}
  
  \begin{thm}{О смешанном произведении векторов взаимного базиса}
    Векторы взаимного базиса направлены по нормалям к соответствующим координатным
    плоскостям. Смешанное произведение векторов взаимного базиса имеет вид:
    \begin{equation*}
      \vr_1 \cdot (\vr_2 \times \vr_3) = \sqrt{g} > 0.
    \end{equation*}
    \noindent Поэтому векторы взаимного базиса некомпланарны и образуют правую
    тройку.
  \end{thm}
  \begin{proof}
    Действительно:
    \begin{align*}
      \vr_1 \cdot (\vr_2 \times \vr_3)
        &= \frac{1}{g\sqrt{g}} ( \vr_2 \times \vr_3 ) \cdot
          \big(
            \underbrace{( \vr_3 \times \vr_1 )}_{\vec{a}}
            \times
            ( \underbrace{\vr_1}_{\vec{b}}
              \times
              \underbrace{\vr_2}_{\vec{c}} )
          \big) \\
        &= \frac{1}{g\sqrt{g}}
          \Big(
            \vr_1
            \underbrace{
              ( \vr_3 \times \vr_1 ) \cdot \vr_2
            }_{\vr_1 \cdot (\vr_2 \times \vr_3) = \sqrt{g}}
            - \vr_2
            \underbrace{
              ( \vr_3 \times \vr_1 ) \cdot \vr_1
            }_{0}
          \Big) \cdot (\vr_2 \times \vr_3) \\
        &= \frac{1}{g} \underbrace{
            \vr_1 \cdot (\vr_2 \times \vr_3)
          }_{\sqrt{g}} = \frac{1}{\sqrt{g}}.
    \end{align*}
  \end{proof}
  
  Векторы основного и взаимного базиса в общем случае изменяются при переходе от
  одной точки пространства к другой. В общем случае эти векторы не являются
  единичными и взаимно перпендикулярными.
  
  
  \subsection*{Компоненты единичного тензора}
  
  Введём в рассмотрение следующие величины:
  \begin{align*}
    g_{k l} &= \vr_k \cdot \vr_l, \\
    g^{k l} &= \vr^k \cdot \vr^l, \\
    g^l_k   &= \vr_k \cdot \vr^l =
      \delta^l_k=
      \begin{cases}
        1, & \text{если } k = l \\
        0, & \text{если } k \neq l
      \end{cases}
  \end{align*}
  \noindent Представляющие собой скалярные произведения векторов основного
  и взаимного базиса, соответствующих одной и той же точке пространства.
  Эти величины изменяются при переходе от одной точки пространства к другой,
  то есть являются функциями криволинейных координат. В дальнейшем покажем,
  что $g_{k l}, g^{k l}, g^l_k$ являются соответственно
  \definition{ковариантными}, \definition{контравариантными} и
  \definition{смешанными компонентами} единичного тензора в базисе криволинейной
  системы координат.
  
  
  \subsection*{Определители Грама}
  
  Для векторов основного и взаимного базиса определители Грама определяется
  следующим образом:
  \begin{align}
    \det (g_{k l})
      &= \det (\vr_k \cdot \vr_l)
      = \Big(\vr_1 \cdot (\vr_2 \times \vr_3) \Big)^2 = g, \\
    \det (g^{k l})
      &= \det (\vr^k \cdot \vr^l)
      = \big(\vr^1 \cdot (\vr^2 \times \vr^3) \big)
      = \frac{1}{g}.
  \end{align}
  
  
  \subsection*{Метрика в криволинейных координатах}
  
  Метрика в криволинейных координатах определяется ковариантными компонентами
  единичного тензора. Под метрикой понимают квадрат элемента длины $ds$:
  \begin{align*}
    & d s^2 = d\vr \cdot d\vr, \\
    & d\vr = \frac{\partial \vr}{\partial x^k} \ d x^k
      = \vr_k \ d x^k.
  \end{align*}
  \noindent Тогда:
  \begin{equation*}
    d s^2 = \vr_k \cdot \vr_l \ d x^k d x^l
      = g_{k l} \ d x^k d x^l.
  \end{equation*}
  \noindent Таким образом, $d s^2$ представляет собой квадратную форму
  дифференциала в криволинейных координатах. Ковариантные компоненты
  единичного тензора являются коэффициентами этой ковариантной формы.
  
  
  \subsection*{Векторные произведения векторов основного и взаимного базиса}
  
  Из равенств
  \begin{align*}
    \vr_k \cdot ( \vr_l \times \vr_s ) &= \sqrt{g} \ e_{k l s}, \\
    \vr^k \cdot ( \vr^l \times \vr^s ) &= \frac{1}{\sqrt{g}} \ e^{k l s}
  \end{align*}
  \noindent вытекают представления векторных произведений базисных векторов:
  \begin{align*}
    \vr_l \times \vr_s &= \sqrt{g} \ e_{k l s} \vr^k, \\
    \vr^l \times \vr_s &= \frac{1}{\sqrt{g}} \ e^{k l s} \vr_k.
  \end{align*}
  \noindent Этот вывод следует из того, что
  \begin{equation*}
    \vr_k \cdot \vr^l = \delta^l_k.
  \end{equation*}


  \section{Цилиндрическая и сферическая системы координат}
  
  \begin{figure}[h!]
    \centering
    \input{Figures/Figure_07}
    \caption{Цилиндрическая и сферическая системы координат}
  \end{figure}
  
  Цилиндрическими координатами называются числа $R, \phi, x_3=z$.
  Сферическими координатами называется тройка чисел $r, \theta, \phi$.
  \begin{align*}
    \vr &= \vi_1 x_1 + \vi_2 x_2 + \vi_3 x_3, \\
    \vec{R} &= \vi_1 x_1 + \vi_2 x_2, \\
    r & \sqrt{x^2_1 + x^2_2 + x^2_3}, \quad
      R = \sqrt{x^2_1 + x^2_1}.
  \end{align*}
  
  \begin{thm}{Радиус-вектор точки пространства в цилиндрической системе координат}
    \begin{equation}
      \vr = R ( \vi_1 \cos \phi + \vi_2 \sin \phi) + \vi_3 z.
    \end{equation}
  \end{thm}
  \begin{proof}
    Выразим вектор основного базиса $\vr_R$:
    \begin{equation*}
      \vr_R = \frac{\partial \vr}{\partial R}
        = \vi_1 \cos \phi + \vi_2 \sin \phi = \vi_R.
    \end{equation*}
    \noindent Получили единичный вектор. Таким образом:
    \begin{equation*}
      g_{R R} = 1.
    \end{equation*}
    
    Выразим вектор основного базиса $\vr_\phi$:
    \begin{equation*}
      \vr_\phi = \frac{\partial \vr}{\partial \phi}
        = R ( -\vi_1 \sin \phi + \vi_2 \cos \phi) = R \vi_\phi.
    \end{equation*}
    \noindent Отсюда получим:
    \begin{equation*}
      g_{\phi \phi} = R^2.
    \end{equation*}
    
    Выразим вектор основного базиса $\vr_z$:
    \begin{equation*}
      \vr_z = \frac{\partial \vr}{\partial z} = \vi_3.
    \end{equation*}
    \noindent Отсюда:
    \begin{equation*}
      g_{z z} = 1.
    \end{equation*}
  \end{proof}
  
  \begin{thm}{Радиус-вектор точки пространства в сферической системе координат}
    \begin{equation}
      \vr = r \sin \theta ( \vi_1 \cos \phi + \vi_2 \sin \phi) + r \cos \theta \vi_3.
    \end{equation}
  \end{thm}
  \begin{proof}
    Выразим вектор основного базиса $\vr_r$:
    \begin{align*}
      \vr_r &= \frac{\partial \vr}{\partial r}
        = \sin \theta ( \vi_1 \cos \phi + \vi_2 \sin \phi) + \vi_3 \cos \theta = \vi_r, \\
      g_{r r} &= 1.
    \end{align*}
    
    Выразим вектор основного базиса $\vr_\theta$:
    \begin{align*}
      \vr_\theta &= \frac{\partial \vr}{\partial \theta}
        = r \cos \theta ( \vi_1 \cos \phi + \vi_2 \sin \phi) - r \sin \theta \vi_3 = r \vi_\theta, \\
      g_{\theta \theta} &= r^2.
    \end{align*}
    
    Выразим вектор основного базиса $\vr_\phi$:
    \begin{align*}
      \vr_\phi &= \frac{\partial \vr}{\partial \phi}
        = r \sin \theta ( -\vi_1 \sin \phi + \vi_2 \cos \phi) = r \sin \theta \vi_\phi, \\
      g_{\phi \phi} &= r^2 \sin^2 \theta.
    \end{align*}
  \end{proof}
  
  Цилиндрическая и сферическая системы координат являются ортогональными,
  то есть векторы основного базиса в этих системах координат
  попарно перпендикулярны.
  
  \begin{figure}[h!]
    \centering
    \input{Figures/Figure_08}
    \caption{Ортогональные базисы в цилиндрической и сферической системах координат}
  \end{figure}
  
  
  \section{Векторы в криволинейных координатах}
  
  Рассмотрим некоторый вектор $\vec{a}$:
  \begin{equation*}
    \vec{a} = \sum^3_{k=1} a_k \vi_k,
  \end{equation*}
  \noindent где $a_k$ -- компоненты вектора $\vec{a}$ в ортонормированном
  базисе. В каждой точке пространства можно ввести основной и взаимный базисы
  криволинейной системы координат. Векторы основного базиса некомпланарны. % TODO: компланарны или некомпланарны?
  Векторы взаимного базиса некомпланарны. Поэтому векторы $\vi_k$ можно
  единственным образом разложить или в основном базисе, или во взаимном.
  Таким образом, для одного и того же вектора $\vec{a}$ справедливы следующие
  разложения:
  \begin{equation*}
    \vec{a}
      = \underbrace{a^k \vr_k}_{\text{контравариантные компоненты}}
      = \underbrace{a_k \vr^k}_{\text{ковариантные компоненты}}.
  \end{equation*}
  
  \definition{Ковариантными} компонентами вектора называются коэффициенты его
  разложения во взаимном базисе. \definition{Контравариантными} компонентами
  вектора называются коэффициенты его разложения в основном базисе.
  В ортонормированном базисе ко- и контравариантные компоненты вектора
  совпадают. Выражения компонент вектора имеют вид:
  \begin{align*}
    a_k &= \vec{a} \cdot \vr_k, \\
    a^k &= \vec{a} \cdot \vr^k.
  \end{align*}
  \noindent Здесь можно видеть, что соблюдается правило
  ``формула сама себя пишет''. Действительно:
  \begin{equation*}
    \vec{a} \cdot \vr^k
      = a^l \underbrace{\vr_l \cdot \vr^k}_{\delta^k_l} = a^k.
  \end{equation*}
  
  Ко- и контравариантные компоненты вектора связаны правилом поднятия и
  опускания индекса:
  \begin{align*}
    a_k &= g_{k l} a^l, \\
    a^k &= g^{k l} a_l.
  \end{align*}
  \noindent Действительно:
  \begin{equation*}
    a^k = \vec{a} \cdot \vr^k = a_l \vr^l \cdot \vr^k = a_l g^{k l}.
  \end{equation*}
  \noindent Очевидно, что коэффициенты $g_{k l} = \vr_k \cdot \vr_l$
  являются ковариантными компонентами вектора $\vr_k$: $\vr_k = g_{k l} \vr^l$.
  Аналогично $g^{k l} = \vr^k \cdot \vr^l$, $\vr^k = g^{k l} \vr_l$.
  Таким образом:
  \begin{align*}
    g_{k l} &= \vr_k \cdot \vr_l, & \vr_k = g_{k l} \vr^l, \\
    g^{k l} &= \vr^k \cdot \vr^l, & \vr^k = g^{k l} \vr_l.
  \end{align*}
  
  \definition{Физическими} компонентами вектора называются его проекции на направления
  векторов основного и взаимного базиса и обозначаются $a_{(k)}$ и $a^{(k)}$.
  \begin{align*}
    a_{(k)} &= \vec{a} \cdot \frac{\vr_k}{|\vr_k|} = \frac{a_k}{\sqrt{g_{k k}}}, \\
    a^{(k)} &= \vec{a} \cdot \frac{\vr^k}{|\vr^k|} = \frac{a^k}{\sqrt{g^{k k}}}.
  \end{align*}
  
  Все уравнения механики формулируются именно в физических компонентах
  векторов и тензоров. Физические компоненты не несут информацию о длине
  базисных векторов. Проиллюстрируем смысл ковариантных, контравариантных
  и физических компонент вектора на примере вектора $\vec{a}$,
  компланарного векторам основного базиса $\vr_1, \vr_2$:
  \begin{equation*}
    \vec{a} = a^1 \vr_1 + a^2 \vr_2.
  \end{equation*}
  \noindent При этом будем считать, что $\vr_1, \vr_2 \perp \vr_3$.
  
  
  \subsection*{Операции в косоугольной системе координат}
  
  \begin{figure}[h!]
    \centering
    \input{Figures/Figure_09} % TODO: Доделать иллюстрацию
    \caption{Операции векторной алгебры в косоугольном базисе}
  \end{figure}
  
  \begin{align*}
    \vec{a} \cdot \vec{b}
      &= a_k \ \underbrace{ \vr^k \cdot \vr_l}_{\delta^k_l} \ b^l
      = a_k b^k = a^k b_k, \\
    \vec{a} \times \vec{b}
      &= a_k \ \vr^k \times \vr^l \ b_l
      = \vr_s \frac{1}{\sqrt{g}} \ e^{s k l} \ a_k b_l, \\
    a^2 &= a_k a^k = g^{k l} \ a_l a_k.
  \end{align*}
  
  
  \section{Тензоры в криволинейных координатах}
  
  При помощи векторов основного и взаимного базиса можно построить
  четыре типа диад. Тогда для тензора второго ранга $\hat{Q}$ справедливы
  следующие диадные разложения:
  \begin{align}
    \hat{Q} &= Q_{k l} \ \vr^k \vr^l \\
      &= Q^{k l} \ \vr_k \vr_l \\
      &= Q^k_{\bullet l} \ \vr_k \vr^l \\
      &= Q^{\bullet k}_{l} \vr^k \vr_l.
  \end{align}
  \noindent Здесь
  \begin{align*}
    & Q_{k l} \text{ ковариантные компоненты тензора}, \\
    & Q^{k l} \text{ контравариантные компоненты}, \\
    & \begin{array}{@{}c@{}}
        Q^k_{\bullet l} \text{ контра-кованиантные компоненты} \\
        Q^{\bullet l}_k \text{ ко-контравариантные компоненты}
      \end{array} \}
      \text{смешанные компоненты}.
  \end{align*}
  
  У одного и того же тензора второго ранга существуют различные
  компоненты четырёх типов.
  
  
  \subsection*{Выражения компонент тензора}
  
  \begin{align}
    & Q_{k l} = \vr_k \cdot \hat{Q} \cdot \vr_l,
      & Q^{k l} = \vr^k \cdot \hat{Q} \cdot \vr^l, \\
    & Q^k_{\bullet l} = \vr^k \cdot \hat{Q} \cdot \vr_l,
      & Q^{\bullet l}_k = \vr_k \cdot \hat{Q} \cdot \vr^l.
  \end{align}
  \noindent Действительно:
  \begin{equation*}
    \vr^k \cdot \hat{Q} \cdot \vr_l
      = \underbrace{\vr^k \cdot \vr_m}_{\delta^k_m} \ 
        \underbrace{\vr^n \cdot \vr_l}_{\delta^n_l} \ 
        Q^m_{\bullet n} = Q^k_{\bullet l}.
  \end{equation*}
  
  Компоненты тензора различных типов связаны правилами поднятия
  и опускания индексов:
  \begin{align}
    Q_{k l} &= g_{k m} \ g_{l n} Q^{m n} = g_{k s} \ Q^k_{\bullet l}, \\
    Q^{k l} &= g^{k m} \ g^{l n} Q_{m n} = g^{k s} \ Q^{\bullet l}_s.
  \end{align}
  
  
  \subsection*{Физические компоненты тензора}
  
  \begin{align}
    Q_{(k l)}
      &= \frac{\vr_k}{|\vr_k|}
        \cdot \hat{Q} \cdot
        \frac{\vr_l}{|\vr_l|}
      = \frac{Q_{k l}}{\sqrt{g_{k k} g_{l l}}}, \\
    Q^{(k l)}
      &= \frac{\vr^k}{|\vr^k|}
        \cdot \hat{Q} \cdot
        \frac{\vr^l}{|\vr^l|}
      = \frac{Q^{k l}}{\sqrt{g^{k k} g^{l l}}}.
  \end{align}
  
  
  \subsection*{Единичный тензор в криволинейных координатах}
  
  Найдём компоненты единичного тензора:
  \begin{align*}
    \vr_k \cdot \hat{g} \cdot \vr_l &= \vr_k \cdot \vr_l = g_{k l}, \\
    \vr^k \cdot \hat{g} \cdot \vr^l &= \vr^k \cdot \vr^l = g^{k l}, \\
    \vr^k \cdot \hat{g} \cdot \vr_l &= \vr^k \cdot \vr_l = \delta^k_l, \\
    \vr_k \cdot \hat{g} \cdot \vr^l &= \vr_k \cdot \vr^l = \delta^l_k.
  \end{align*}
  \noindent Отсюда, диадное разложение единичного тензора имеет вид:
  \begin{equation}
    \hat{g} = \delta^k_l \ \vr_k \vr^l = \vr_k \vr^k.
  \end{equation}
  \noindent Таким образом
  \begin{align}
    & \hat{g} = \vr_k \vr^k = \vr^k \vr_k, \\
    & \hat{g} = g_{k l} \ \vr^k \vr^l = g^{k l} \vr_k \vr_l.
  \end{align}
  
  
  \subsection*{Операции тензорной алгебры}
  
  \begin{align}
    & \hat{Q} \cdot \vec{a}
      = Q_{k l} \vr^k \vr^l \cdot \vec{a}
      = Q_{k l} a^l \vr^k, \\
    & \hat{Q} \cdot \hat{P}
      = Q_{k l} \ \vr^k
        \underbrace{\vr^l \cdot \vr_m}_{\delta^l_m}
        \vr_n \ P^{m n}
      = Q_{k l} P^{l n} \vr^k \vr_n, \\
    & \hat{Q} \cdot \cdot \hat{P}
      = \Sp (\hat{Q} \cdot \hat{P})
      = Q_{k l} \ P^{l n}
        \underbrace{\vr^k \cdot \vr_n}_{\delta^k_n}
      = Q_{k l} \ P^{l k}, \\
    & \Sp \hat{Q}
      = \hat{g} \cdot \hat{Q}
      = g_{k l} \ Q^{l k}
      = Q^k_{\bullet k} = Q^{\bullet k}_k.
  \end{align}
  
  Вычислим третий инвариант тензора. Свободное слагаемое характеристического
  выражения с коэффициентом $-1$ при старшей степени $\lambda$.
  \begin{align*}
    & \det ( \hat{Q} - \lambda \hat{g} ) = P(\lambda), \\
    & I_3(\hat{Q})
      = \det (Q^{\bullet l}_k)
      = \det (Q^k_{\bullet l}), \\
    & \hat{Q} - \lambda \hat{g}
      = (Q^{\bullet l}_k - \lambda \delta^l_k) \vr^k \vr_l
      = (Q^k_{\bullet l} - \lambda \delta^k_l) \vr_k \vr^l, \\
    & \hat{Q} - \lambda \hat{g}
      = (Q_{k l} - \lambda g_{k l}) \vr^k \vr^l
      = (Q^{k l} - \lambda g^{k l}) \vr_k \vr_l.
  \end{align*}
  
  
  \section{Ортогональные криволинейные координаты}
  
  Криволинейные координаты называются \emph{ортогональными}, если
  векторы основного базиса попарно перпендикулярны
  ($\vr_k \cdot \vr_l=0, \ k \neq l$). Модули векторов основного базиса
  в случае ортогональных криволинейных координат называются
  \emph{коэффициентами Ляме}:
  \begin{align}
    & H_k = |\vr_k| = \sqrt{g_{k k}}, \\
    & \vr_k = H_k \vec{\jmath}_k,
  \end{align}
  \noindent где $\vec{\jmath}$ -- единичные векторы,
  $\vec{\jmath}_k=\frac{\vr_k}{|\vr_k|}$.
  
  Найдём якобиан и векторы взаимного базиса:
  \begin{equation}
    \sqrt{g}
      = \vr_1 \cdot ( \vr_2 \times \vr_3)
      = H_1 H_2 H_3.
  \end{equation}
  \noindent Векторы взаимного базиса:
  \begin{equation*}
    \vr^1
      = \frac{1}{\sqrt{g}} \vr_2 \times \vr_3
      = \frac{1}{H_1 H_2 H_3} \ H_2 H_3 \ \vec{\jmath}_2 \times \vec{\jmath}_3
      = \frac{\vec{\jmath}_1}{H_1}.
  \end{equation*}
  \noindent Таким образом:
  \begin{equation}
    \vr^k = \frac{\vec{\jmath}_k}{H_k}.
  \end{equation}
  
  % TODO:                              (?)
  В криволинейных координатах существуют компоненты векторов и тензоров
  только первого типа:
  \begin{align}
    & a_{(k)} = \vec{a} \cdot \vec{\jmath}_k, \\
    & Q_{(k l)} = \vec{\jmath}_k \cdot \hat{Q} \cdot \vec{\jmath}_k.
  \end{align}
  \noindent Ко- и контравариантные компоненты выражаются через физические:
  \begin{align}
    & a_k = H_k a_{(k)}, \\
    & a^k = \frac{a_{(k)}}{H_k}.
  \end{align}
  \noindent Аналогично для тензора:
  \begin{align}
    & Q_{k l} = H_k H_l Q_{(k l)}, \\
    & Q^{k l} = \frac{Q_{(k l)}}{H_k H_l}.
  \end{align} 
