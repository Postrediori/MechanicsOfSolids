  \chapter{Тензорный анализ в криволинейных координатах}
  
  Векторы основного и взаимного базиса в общем случае изменяются при переходе
  от одной точки пространства к другой. Поэтому их производные отличны от нуля.
  
  
  \subsection*{Производные векторов основного базиса}
  
  \begin{equation}
    \vr_{k l}
      = \frac{\partial \vr_k}{\partial x^l}
      = \frac{\partial^2 \vr}{\partial x^l \partial x^k}
      = \frac{\partial \vr_l}{\partial x_k}
      = \vr_{l k}.
  \end{equation}
  
  Разложим производные векторов основного базиса в основном базисе:
  
  \begin{equation}
    \vr_{k l} = \Gamma^s_{k l} \vr_s,
  \end{equation}
  \noindent где $\Gamma^s_{k l}$ -- компоненты контравариантных производных
  векторов основного базиса,
  \begin{equation}
    \Gamma^s_{k l} = \vr_{k l} \cdot \vr^s.
  \end{equation}
  
  Контравариантные компоненты производных векторов основного базиса называются
  \definition{символами Кристоффеля второго рода}. Символы Кристоффеля симметричны
  относительно перестановки нижних индексов.
  
  
  \subsection*{Производные векторов взаимного базиса}
  
  Для вычисления производных векторов взаимного базиса продифференцируем по
  криволинейным координатам равенство:
  \begin{align}
    & \vr^k \cdot \vr_l = \delta^k_l, \\
    & \frac{\partial \vr^k}{\partial x_s} \cdot \vr_l
      + \underbrace{\vr^k \cdot \vr_{l s}}_{\Gamma^k_{l s}} = 0, \\
    & \frac{\partial \vr^s}{\partial x_s} \cdot \vr_l = - \Gamma^k_{s l}.
  \end{align}
  
  Символы Кристоффеля второго рода, взятые с противоположным знаком являются
  ковариантными компонентами производных векторов взаимного базиса, то есть
  коэффициентами их разложения во взаимном базисе.
  
  \begin{equation}
    \frac{\partial \vr^k}{\partial x^s} = - \Gamma^k_{s l} \vr^l.
  \end{equation}
  
  Символы Кристоффеля второго рода однозначно определяются ковариантными
  компонентами единичного тензора, заданными как функции координат
  $g_{k l} (x^1, x^2, x^3)$. Иными словами, символы Кристоффеля однозначно
  определяются метрикой пространства.
  
  \begin{align}
    & \Gamma^s_{k l} = \vr_{k l} \cdot \vr^s = g^{m s} \vr_{k l} \cdot \vr_m, \\
    & \frac{\partial}{\partial x^l} ( \vr_k \cdot \vr_m )
      = \vr_{k l} \cdot \vr_m + \vr_{m l} \cdot \vr_k
      = \frac{\partial g_{k m}}{\partial x_l}, \\
    & \frac{\partial}{\partial x^m} ( \vr_l \cdot \vr_k )
      = \vr_{l m} \cdot \vr_k + \vr_{k m} \cdot \vr_l
      = \frac{\partial g_{k l}}{\partial x_m}, \\
    & \frac{\partial}{\partial x^k} ( \vr_m \cdot \vr_l )
      = \vr_{k m} \cdot \vr_l + \vr_{k l} \cdot \vr_m
      = \frac{\partial g_{m l}}{\partial x_k}.
  \end{align}
  
  Сложим первое и третье равенства и вычтем их них второе, получим:
  
  \begin{align}
    & 2 \vr_{k l} \cdot \vr_m
      = \frac{\partial g_{k m}}{\partial x^l}
        + \frac{\partial g_{l m}}{\partial x^k}
        - \frac{\partial g_{k l}}{\partial x^m}. \\ 
    \intertext{Следовательно,}
    & \Gamma^s_{k l}
      = \frac{1}{2} g^{m s}
        \Big(
          \frac{\partial g_{k m}}{\partial x^l}
          + \frac{\partial g_{l m}}{\partial x^k}
          - \frac{\partial g_{k l}}{\partial x^m}
        \Big).
  \end{align}
  
  Вычислим производную якобиана $\sqrt{g}$ по криволинейной координате:
  
  \begin{align}
    \begin{split}
      \frac{\partial}{\partial x^k} \sqrt{g}
        &= \frac{\partial}{\partial x^k}
          \big( \vr_1 \cdot (\vr_2 \times \vr_3) \big) \\
        &= \vr_{1 k} \cdot ( \vr_2 \times \vr_3 )
          + \vr_1 \cdot ( \vr_{2 k} \times \vr_3 )
          + \vr_1 \cdot ( \vr_2 \times \vr_{3 k} ) \\
        &= \Gamma^s_{1 k} \vr_s \cdot ( \vr_2 \times \vr_3 )
          + \Gamma^s_{2 k} \vr_1 \cdot ( \vr_s \times \vr_3 )
          + \Gamma^s_{3 k} \vr_1 \cdot ( \vr_2 \times \vr_s ) \\
        &= \Gamma^1_{1 k} \vr_1 \cdot ( \vr_2 \times \vr_3 )
          + \Gamma^1_{2 k} \vr_1 \cdot ( \vr_2 \times \vr_3 )
          + \Gamma^1_{3 k} \vr_1 \cdot ( \vr_2 \times \vr_3 ) \\
        &= \Gamma^s_{s k} \sqrt{g}.
    \end{split} \\
    \frac{\partial \sqrt{g}}{\partial x^k} &= \Gamma^s_{s k} \sqrt{g}.
  \end{align}
  
  
  \section{Ковариантное дифференцирование}
  
  Изменение тензорного поля при переходе от одной точки пространства к другой
  определяются исключительно свойствами этого тензорного поля (инвариантного
  объекта) и не зависит от выбранной системы координат.
  
  \begin{equation}
    d \hat{Q} = d \vr \cdot \vec{\nabla} \hat{Q}.
  \end{equation}
  
  Однако базисные векторы в выбранной системе координат изменяются при переходе
  от одной точки к другой. Поэтому, компоненты тензорного поля несут информацию
  не только о тензорном поле как о инвариантном объекте, но и об изменении
  базисных векторов. Например, если $\vec{a}=\const,\ d\vec{a}=0$, то компоненты
  этого вектора $a^k, a_k$ не являются постоянными, так как базисные векторы
  изменяются.
  
  Изменение компонент тензорного поля определяется не только изменением
  тензорного поля как инвариантного объекта, но и изменением базиса. В связи с
  этим возникает необходимость ввести такие характеристики изменения компонент
  тензорного поля, которые учитывали бы изменения векторов базиса. Этими
  характеристиками являются ковариантные (абсолютные) производные.
  
  
  \subsection*{Ковариантные производные компонент векторного поля}
  
  \begin{align}
    \begin{split}
      \frac{\partial \vec{a}}{\partial x^k}
        &= \frac{\partial}{\partial x^k} ( a^l \vr_l ) \\
        &= \frac{\partial a^l}{\partial x^k} \vr_l
          + a^l \vr_{l k} \\
        &= \frac{\partial a^l}{\partial x^k} \vr_l
          + \underbrace{\Gamma^s_{k l} a^l \vr_s}_{s \longleftrightarrow l} \\
        &= \underbrace{
          \Big(
            \frac{\partial a^l}{\partial x^k}
            + \Gamma^l_{k s} a^s
          \Big) }_{\text{ковариантная производная } a^l_{,k}=\nabla_k a^l}
          \vr_l.
    \end{split} \\
    \frac{\partial \vec{a}}{\partial x^k}
      &= \frac{\partial}{\partial x^k} ( a_l \vr^l ) \\
      &= \frac{\partial a_l}{\partial x^k} \vr^l
        - \Gamma^l_{k s} a_l \vr^s \\
      &= \underbrace{
        \Big(
          \frac{\partial a_l}{\partial x^k}
          - \Gamma^s_{k l} a_s
        \Big) }_{
          \text{ковариантная производная от ковариантной компоненты }
          a_{l,k}=\nabla_k a_l }
        \vr^l.
  \end{align}
  
  Отсюда вывод:
  \begin{align}
    & \frac{\partial \vec{a}}{\partial x^k}
      = a^l_{,k} \vr_l
      = a_{l,k} \vr^l, \\
    \intertext{где}
    & a^l_{,k} = \frac{\partial a^l}{\partial x^k}
      + \Gamma^l_{k s} a^s, \\
    & a_{l,k} = \frac{\partial a_l}{\partial x^k}
      - \Gamma^s_{k l} a_s.
  \end{align}
  
  Для постоянного вектора $\vec{a}$ имеет место равенство
  $\frac{\partial \vec{a}}{\partial x^k}=0$ и ковариантные производные его
  компонент тоже равны нулю, а частные производные не равны нулю. Для
  прямоугольной декартовой системы координат все символы Кристоффеля равны нулю
  и ковариантные производные превращаются в обычные частные производные
  по координатам.
  
  
  \subsection*{Ковариантные производные компонент тензора второго ранга}
  
  \begin{equation}
    \begin{split}
      \frac{\partial \hat{Q}}{\partial x^k}
        &= \frac{\partial}{\partial x^k}
          \Big(
            Q^{m n} \vr_m \vr_n
          \Big) \\
        &= \frac{\partial Q^{m n}}{\partial x^k} \vr_m \vr_n
          + \underbrace{
            \Gamma^s_{k m} Q^{m n} \vr_s \vr_n
          }_{s \longleftrightarrow m}
          + \underbrace{
            \Gamma^s_{k n} Q^{m n} \vr_m \vr_s
          }_{s \longleftrightarrow n} \\
        &= \underbrace{
          \Big(
            \frac{\partial Q^{m n}}{\partial x^k}
            + \Gamma^m_{k s} Q^{s n}
            + \Gamma^n_{k s} Q^{m s}
          \Big) }_{Q^{m n}_{,k}} \vr_m \vr_n,
    \end{split}
  \end{equation}
  \noindent где $Q^{m n}_{,k}$ -- ковариантная производная по $k$.
  
  \begin{equation}
    \begin{split}
      \frac{\partial \hat{Q}}{\partial x^k}
        &= Q^{m n}_{,k} \vr_m \vr_n \\
        &= Q_{mn,k} \vr^m \vr^n \\
        &= Q^m_{\bullet n,k} \vr_m \vr^n \\
        &= \underbrace{
          Q^{\bullet n}_{m,k} \vr^m \vr_n
          }_{\text{ко-ковариантные производные от ко-ковариантныз компонентов}},
    \end{split}
  \end{equation}
  
  \noindent где ковариантные производные соответствующие компонентам тензора
  выражаются равенствами:
  
  \begin{align}
    & Q^{m n}_{,k}
      = \frac{\partial Q^{m n}}{\partial x^k}
        + \Gamma^m_{k s} Q^{s n}
        + \Gamma^n_{k s} Q^{m s}, \\
    & Q_{mn,k}
      = \frac{\partial Q_{m n}}{\partial x^k}
        - \Gamma^s_{k m} Q_{s n}
        - \Gamma^s_{k n} Q_{m s}, \\
    & Q^m_{\bullet n,k}
      = \frac{\partial Q^m_{\bullet n}}{\partial x^k}
        + \Gamma^m_{s k} Q^s_{\bullet n}
        - \Gamma^s_{n k} Q^m_{\bullet s}.
  \end{align}
  
  \begin{thm}{Теорема Риччи}
    Ковариантные производные компонент единичного тензора равны нулю:
    \begin{align}
      g^{k l}_{,s} &= 0, \\
      g_{k l, s} &= 0.
    \end{align}
  \end{thm}
  \begin{proof}
    Так как единичный тензор $\hat{g}$ является постоянным, то:
    \begin{equation}
      \frac{\partial \hat{g}}{\partial x^s} = 0.
    \end{equation}
  \end{proof}
  
  Аналогично, ковариантные производные компонент тензора Леви-Чивита
  равны нулю:
  \begin{equation}
    \tensor[^3]{\hat{e}}{} = -\hat{g} \times \hat{g} = \const.
  \end{equation}
  
  
  \section{Набла-оператор в криволинейных координатах}
  
  Рассмотрим векторное поле $\vec{a}=\vec{a}(\vr)$ и сравним два представления
  дифференциала этого векторного поля:
  
  \begin{align}
    & d \vec{a} = \frac{\partial \vec{a}}{\partial x^k} d x^k, \\
    & d \vec{a} = d \vr \cdot \vec{\nabla} \vec{a}
      = \frac{\partial \vr}{\partial x^k}
        d x^k \cdot \vec{\nabla} \vec{a}
      = \vr_k \cdot \vec{\nabla} \vec{a} \ dx^k. \\  
  \intertext{В силу произвольности дифференциала $dx^k$ получим: }
    & \vr_k \cdot \vec{\nabla} \vec{a}
      = \frac{\partial \vec{a}}{\partial x^k}. \\
  \intertext{Отсюда: }
    & \underbrace{
      \overbrace{
        \vr^k \vr_k
      }^{\hat{g}}
      \cdot \vec{\nabla} \vec{a}
    }_{\vec{\nabla}\vec{a}}
      = \vr^k \frac{\partial \vec{a}}{\partial x^k}, \\
    & \vec{\nabla} \vec{a} = \vr^k \frac{\partial \vec{a}}{\partial x^k}.
  \end{align}
  
  Отсюда вытекает представление набла-оператора в криволинейных координатах:
  
  \begin{equation}
    \vec{\nabla} = \vr^k \frac{\partial}{\partial x^k}.
  \end{equation}
  
  Аналогичные рассуждения можно провести для скалярного поля $d f$:
  
  \begin{align}
    & d f = \frac{\partial f}{\partial x^k} d x^k, \\
    & d f = \vec{\nabla} f \cdot d \vec{r}
      = \vec{\nabla} f \cdot \vr_k \ d x^k, \\
    & \vec{\nabla} f \cdot \vr_k
      = \frac{\partial f}{\partial x^k}.
  \end{align}
  
  Следовательно, частные производные $\frac{\partial f}{\partial x^k}$
  являются ковариантными компонентами вектора
  
  \begin{equation}
    \vec{\nabla} f
      = \vr^k \frac{\partial f}{\partial x^k}.
  \end{equation}
  
  
  \section{Дифференциальные операции над тензорными полями в криволинейных координатах}
  
  Полученное представление набла-оператора в сочетании с ковариантным
  дифференцированием позволяет вычислять дифференциальные операции над тензорными
  полями в криволинейных координатах.
  
  \subsection*{Дивергенция векторного поля}
  
  \begin{align}
    \operatorname{div} \vec{a} &= \vec{\nabla} \cdot \vec{a}
      = \vr^k \cdot \frac{\partial \vec{a}}{\partial x^k}
      = \vr^k \cdot \frac{\partial}{\partial x^k}
        ( a^l \vr_l )
      = \underbrace{\vr^k \cdot \vr_l}_{\delta^k_l}
        a^l_{,k}, \\
    \operatorname{div} \vec{a} &= a^k_{,k}
      = \frac{\partial a^k}{\partial x^k} + \Gamma^k_{s k} a^s.
  \end{align}
  
  \subsection*{Градиент векторного поля}
  
  \begin{equation}
    \vec{\nabla} \vec{a} = \vr^k \frac{\partial \vec{a}}{\partial x^k}
      = \vr^k \frac{\partial}{\partial x^k} ( a_l \vr^l )
      = \vr^k \vr^l a_{l,k},
  \end{equation}
  \begin{equation}
    \begin{split}
      \operatorname{def} \vec{a}
        &= \frac{1}{2} \big( ( \vec{\nabla} \vec{a} )^T + \vec{\nabla} \vec{a} \big)
        = \frac{1}{2} ( a_{l,k} + a_{k,l} ) \vr^k \vr^l \\
        &= \Big( \frac{1}{2} \big(
            \frac{\partial a_k}{\partial x^l} + \frac{\partial a_l}{\partial x^k}
          \big) - \Gamma^s_{l k} a_s \Big) \vr^k \vr^l.
    \end{split}
  \end{equation}
  
  \subsection*{Ротор векторного поля}
  
  \begin{equation}
    \begin{split}
      \rot \vec{a} &= \vec{\nabla} \times \vec{a}
        = \vr^k \times \frac{\partial \vec{a}}{\partial x^k}
        = \vr^k \times \frac{\partial}{\partial x^k} ( a_l \vr^l ) \\
        &= \vr^k \times \vr^l a_{l,k}
        = \vr_s \frac{1}{\sqrt{g}} e^{s k l} \Big(
            \frac{\partial a_l}{\partial x^k} - \Gamma^m_{k l} a_m
          \Big).
    \end{split}
  \end{equation}

  \noindent Очевидно, что $e^{s k l} \Gamma^m_{k l} = 0$. Окончательно получаем:
  
  \begin{equation}
    \vec{\nabla} \times \vec{a}
      = \vr_s \frac{1}{\sqrt{g}} e^{s k l}
        \frac{\partial a_l}{\partial x^k}.
  \end{equation}
  
  \subsection*{Дивергенция тензорного поля}
  
  \begin{equation}
    \begin{split}
      \operatorname{div} \hat{Q} &= \vec{\nabla} \cdot \hat{Q}
        = \vr^k \cdot \frac{\partial \hat{Q}}{\partial x^k} \\
        &= \vr^k \cdot \frac{\partial}{\partial x^k}
          ( Q^{m n} \vr_m \vr_n )
        = \underbrace{ \vr^k \cdot \vr_m }_{\delta^k_m}
          \ \vr_n Q^{m n}_{,k} = Q^{m n}_{,m} \vr_n,
    \end{split}
  \end{equation}
  \begin{equation}
    \operatorname{div} \hat{Q} = \Big(
        \frac{\partial Q^{m n}}{\partial x^m}
        + \Gamma^m_{s m} Q^{s n}
        + \Gamma^n_{s m} Q^{m s}
      \Big) \vr_n.
  \end{equation}
  
  \subsection*{Лапласиан скалярного поля}
  
  \begin{equation}
    \begin{split}
      \nabla^2 f
        &= \vec{\nabla} \cdot \vec{\nabla} f
        = \vr^k \cdot \frac{\partial}{\partial x^k}
          \big( \vr^l \frac{\partial f}{\partial x^l} \big) \\
        &= \underbrace{\vr^k \cdot \vr^l}_{g^{k l}}
          \frac{\partial^2 f}{\partial x^k \partial x^l}
          + \vr^k \cdot
            \frac{\partial \vr^l}{\partial x^k}
            \frac{\partial f}{\partial x^l} \\
        &= g^{k l} \frac{\partial^2 f}{\partial x^k \partial x^l}
          - \underbrace{\vr^k \cdot \vr^s}_{g^{k s}}
          \Gamma^l_{s k} \frac{\partial f}{\partial x^l} \\
        &= g^{k l} \Big(
            \frac{\partial^2 f}{\partial x^k \partial x^l}
            - \Gamma^s_{k l} \frac{\partial f}{\partial x^s}
          \Big).  
    \end{split}
  \end{equation}
  
  
  \section{Условия интегрируемости в дифференциальной форме}
  
  Рассмотрим дифференциальную форму $d'f$:
  
  \begin{equation}
    d'f = f_k ( x^1, x^2, x^3 ) d x^k,
  \end{equation}
  
  \noindent где $f_k$ -- непрерывно дифференцируемые функции координат. Для
  обозначения дифференциальной формы используется символ $d'$, тем самым
  подчёркивается, что в общем случае рассматриваемое выражение не является
  полным дифференциалом какой-либо функции $f$.
  
  Если же дифференциальная форма является полным дифференциалом, то есть
  имеет место равенство $f_k dx^k = df$, то
  
  \begin{align}
    & f_k = \frac{\partial f}{\partial x^k}. \\
    \intertext{Тогда }
    & \frac{\partial f_k}{\partial x^l} = \frac{\partial f_l}{\partial x^k}.
  \end{align}
  
  Последние равенства выражают необходимое и достаточное условие интегрируемости
  дифференциальной формы. Аналогичный результат получается и для векторной
  дифференциальной формы:
  
  \begin{equation}
    d'\vec{a} = \vec{a}_k ( x^1, x^2, x^3 ) d x^k.
  \end{equation}
  
  \noindent Необходимым и достаточным условием её интегрируемости является
  выполнение равенства
  \begin{equation}
    \frac{\partial \vec{a}_k}{\partial x^l}
      = \frac{\partial \vec{a}_l}{\partial x^k}.
  \end{equation}
  
  
  \section{Тензор Римана-Кристоффеля}
  
  Трёхмерное пространство называется \definition{евклидовым}, если положение любой
  точки пространства определяется при помощи радиуса-вектора $\vr$, который
  откладывается из начала единой для всего пространства декартовой прямоугольной
  системы координат. Радиус-вектор $\vr$ определяется следующим образом:
  
  \begin{equation}
    \vr = \sum^3_{k=1} \vi_k x_k ( x^1, x^2, x^3 ).
  \end{equation}
  
  \noindent Метрика в евклидовом пространстве определяется равенством:
  
  \begin{equation}
    \begin{split}
      ds^2 &= dx^2_1+dx^2_2+dx^2_3 \\
        &= g_{k l} (x^1, x^2, x^3) \ d x^k d x^l,
    \end{split}
  \end{equation}
  
  \noindent где
  
  \begin{align}
    g_{k l}
      &= \frac{\partial \vr}{\partial x^k} \cdot
        \frac{\partial \vr}{\partial x^l} \\
    g_{k l}
      &= \sum^3_{s=1}
        \frac{\partial x_s}{\partial x^k}
        \frac{\partial x_s}{\partial x^l}.
  \end{align}
  
  \noindent Функции $g_{k l} (x^1, x^2, x^3)$ однозначно определяются
  заданием криволинейных координат.
  
  
  \subsection*{Риманово пространство}
  
  Пусть задана произвольная симметричная положительно-определённая
  квадратичная форма:
  
  \begin{equation}
    d s^2 = g_{kl} (x^1,x^2,x^3) \ dx^k dx^l.
  \end{equation}
  
  \noindent В этом случае говорят, что определено
  \definition{трёхмерное риманово пространство}, а тройка чисел $x^1, x^2, x^3$
  определяет точку риманова пространства. Квадратичная форма $d s^2$ определяет
  метрику в римановом пространстве. Возникает вопрос об условиях вырождения
  риманова пространства в евклидово.
  
  Если заданы функции $g_{kl} (x^1,x^2,x^3)$, то соотношение
  
  \begin{equation}
    \frac{\partial \vr}{\partial x^k} \cdot
      \frac{\partial \vr}{\partial x^l} = g_{k l}
  \end{equation}
  
  \noindent можно рассматривать как систему шести дифференциальных уравнений
  в частных производных относительно векторной функции $\vr$
  ($\vr$ -- радиус-вектор точки пространства). Если система имеет решение,
  то риманово пространство вырождается в евклидово. Эти уравнения можно
  сформулировать относительно неизвестных декартовых координат:
  
  \begin{equation}
    \sum^3_{s = 1} \frac{\partial x_s}{\partial x^k}
      \frac{\partial x_s}{\partial x^l} = g_{k l}.
  \end{equation}
  
  \noindent Получили систему шести скалярных уравнений относительно трёх
  неизвестных скалярных функций $x_s$. Система является неопределённой.
  Поэтому решение системы существует только при некоторых дополнительных
  ограничениях, которые должны накладываться на заданные функции
  $g_{kl} (x^1,x^2,x^3)$. Эти ограничения и представляют собой условия
  вырождения риманова пространства в евклидово.
  
  Условия вырождения риманова пространства в евклидово получим как условие
  интегрируемости дифференциальных форм:
  
  \begin{align}
    d \vr = \vr_k d x^k, \\
    d \vr_k = \vr_{k l} d x^l.
  \end{align}
  
  Заданные функции $g_{k l}$ однозначно определяют символы Кристоффеля
  как функции координат:
  
  \begin{align}
    & \Gamma^s_{k l} = \Gamma^s_{k l} ( x^1, x^2, x^3 ), \\
    & \Gamma^s_{k l} = \Gamma^s_{l k}.
  \end{align}
  
  \noindent Последние равенства обеспечивают интегрируемость
  дифференциальной формы $d \vr$:
  
  \begin{equation}
    \frac{\partial \vr_k}{\partial x^l}
      = \frac{\partial \vr_l}{\partial x^k}.
  \end{equation}
  
  Итак, при условии существования функции $\vr_k$ существует функция $\vr$.
  Условие существования функции $\vr_k$ представляет собой условие
  интегрируемости дифференциальной формы $d \vr_k$:
  
  \begin{equation}
    d \vr_k = \vr_{k l} d x^l = \Gamma^s_{k l} \vr_s d x^l.
  \end{equation}
  
  Условие интегрируемости дифференциальной формы $d \vr_k$ имеет вид:
  
  \begin{equation}
    \frac{\partial}{\partial x^l}
      \big(
        \Gamma^s_{k m} \vr_s
      \big) = 
    \frac{\partial}{\partial x^m}
      \big(
        \Gamma^s_{k l} \vr_s
      \big).
  \end{equation}
  
  \noindent При выполнении этих условий существует функция $\vr_k$.
  
  Преобразуем условие интегрируемости:
  
  \begin{align*}
    & \frac{\partial \Gamma^s_{k m}}{\partial x^l} \vr_s
      - \frac{\partial \Gamma^s_{k l}}{\partial x^m} \vr_s
      + \underbrace{
          \Gamma^s_{k m} \frac{\partial \vr_s}{\partial x^l}
        }_{ \Gamma^n_{s l} \vr_n }
      - \underbrace{
          \Gamma^s_{k l} \frac{\partial \vr_s}{\partial x^m}
        }_{ \Gamma^n_{s m} \vr_n } = 0, \\
    & \Big(
        \frac{\partial \Gamma^s_{k m}}{\partial x^l}
        - \frac{\partial \Gamma^s_{k l}}{\partial x^m}
        + \Gamma^n_{k m} \Gamma^s_{n l}
        - \Gamma^n_{k l} \Gamma^s_{n m}
      \Big) \vr_s = 0, \\
    & \underbrace{ \Big(
        \frac{\partial \Gamma^s_{k m}}{\partial x^l}
        - \frac{\partial \Gamma^s_{k l}}{\partial x^m}
        + \Gamma^n_{k m} \Gamma^s_{n l}
        - \Gamma^n_{k l} \Gamma^s_{n m}
      \Big) g_{s p} }_{ R_{k l m s} } \vr^p = 0.
  \end{align*}
  
  \noindent Так как символы Кристоффеля однозначно определяются заданными
  функциями $g_{k l}$, то величины $R_{k l m s}$ также однозначно определяются
  заданными функциями $g_{k l}$. Таким образом, условие вырождения риманова
  пространства в евклидово принимает вид:
  
  \begin{equation}
    R_{k l m s} = 0.
  \end{equation}
  
  Величины $R_{k l m s}$ являются компонентами тензора четвёртого ранга:
  
  \begin{equation}
    \tensor[^4]{\hat{R}}{} = R_{k l m s} \vr^k \vr^l \vr^m \vr^s.
  \end{equation}
  
  \noindent Этот тензор называется \definition{тензором Римана-Кристоффеля}.
  Легко заметить, что из 81 компонент тензора Римана-Кристоффеля независимыми
  являются только 6. Поэтому тензору Римана-Кристоффеля ставится в соответствие
  симметричный тензор второго ранга, называемый \definition{тензором Риччи}:
  
  \begin{equation}
    \hat{R} = \frac{1}{4 g} R_{k l m s} \ 
      \vr^k \times \vr^l \ \vr^m \times \vr^s.
  \end{equation}
  
  Независимые компоненты тензора Риччи совпадают с соответствующими компонентами
  тензора Римана-Кристоффеля. Окончательно, условие вырождения риманова
  пространства в евклидово принимает следующий вид:
  
  \begin{equation}
    R_{k l} = 0.
  \end{equation}
